const port = 3000;

var http = require('http');

var mysql = require('mysql');
var connection = mysql.createConnection({
  user: 'root',
  password: 'secret',
  host: process.env.DATABASE_HOST || '127.0.0.1'
});

http = require('http');
connection.connect();

var dbQuery = "CREATE SCHEMA IF NOT EXISTS studentsdb;";
connection.query(dbQuery, function(err, result) {
  if (!err)
    console.log('Data Base Query executed... ');
  else
    console.log('Error while performing Data Base Query.',err);
});

var tbQuery = "CREATE TABLE IF NOT EXISTS studentsdb.students (id VARCHAR(255),name VARCHAR(255), lname VARCHAR(255) , PRIMARY KEY (\`id\`))";
connection.query(tbQuery, function(err, result) {
  if (!err)
    console.log('Table Query executed... ');
  else
    console.log('Error while performing Table Query.',err);
});

http.createServer(function (req, res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET,POST');
    res.setHeader('Access-Control-Allow-Headers', '*');

    if (req.method == 'POST') {
        var body = '';
        req.on('data', function (data) {
            body += data;
            var reqBody = JSON.parse(body);
            var query = 'INSERT INTO studentsdb.students VALUES (\''+reqBody.id+
                                                    '\', \''+reqBody.name+
                                                    '\', \''+reqBody.lname+
                                                    '\');'
            console.log("Post Received Body: " + query);
            connection.query(query, function(err, rows, fields) {
                if (!err) {
                    //console.log('Query result', rows);
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.end("Student "+reqBody.id+" saved!");
                }
                else {
                    res.writeHead(500, {'Content-Type': 'text/html'});
                    res.end("Internal Server Error");
                    console.log('Error while performing Query.',err);
                }
            });
        });
        req.on('end', function () {
            
            
        });
    } else {
        connection.query('SELECT * FROM studentsdb.students;', function(err, rows, fields) {
            if (!err) {
                console.log('Query result', rows);
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.end(JSON.stringify(rows));
            }
            else {
                res.writeHead(500, {'Content-Type': 'text/html'});
                res.end("Internal Server Error");
                console.log('Error while performing Query.',err);
            }
        });

    }
    
  
}).listen(port);
console.log("Running server at port "+port);
 

