
var serverURL = '/db';



function getUsers() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", serverURL, false );
    xmlHttp.send( null );
    var students = JSON.parse(xmlHttp.responseText);
    //console.log(students);
    var tbl = document.getElementById('table');
    tbl.style.width = '100%';
    tbl.setAttribute('border', '1');
    var tbdy = document.getElementById('tbody');
    tbdy.innerHTML = '';
    for (var key in students) {
      //console.log(key, students[key]);
      var tr = document.createElement('tr');
      var td = document.createElement('td');
      td.appendChild(document.createTextNode(students[key].id))
      tr.appendChild(td)
      tbdy.appendChild(tr);
      var td1 = document.createElement('td');
      td1.appendChild(document.createTextNode(students[key].name))
      tr.appendChild(td1)
      tbdy.appendChild(tr);
      var td2 = document.createElement('td');
      td2.appendChild(document.createTextNode(students[key].lname))
      tr.appendChild(td2)
      tbdy.appendChild(tr);
    }

    tbl.appendChild(tbdy);
}
