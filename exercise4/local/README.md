# ITZData Exercise 4

Exercise consists on having a NodeJS application that will run on 2 nodes, load balancing managed by NGINX, service discovery managed by CONSUL and storage managed by REDIS.

## How to deploy app locally using Docker compose 

### Build Docker Images

```
docker-compose build
```

### Run Docker Compose

```
docker-compose run
```

### Test on localhost

Open your favorite browser and navigate to 

```
http://127.0.0.1/
```