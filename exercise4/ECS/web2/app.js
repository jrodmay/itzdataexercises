var consul = require('consul')({"host":"127.0.0.1","port":8500});
var http = require('http');
var redis = require('redis');
var redisClient;
var redisAddress;
var redisPort;
const port = 8888;


// Get Redis Service information from Consul
consul.health.service('redis', function(err, result) {
	redisAddress = result[0].Service.Address;
	redisPort = result[0].Service.Port;
	console.log("Connection ",redisAddress ," : ",redisPort );

	// Create Redis Client with info obtained 
	redisClient = createClient(redisPort,redisAddress);

	// Set a key - value on redis 
	redisClient.set('greeting', 'Hello ITZData, from Redis...');

  	if (err) throw err;
});


function createClient(port, host) {

    var client = redis.createClient(Number(redisPort), redisAddress+"");
    return client;
}


http.createServer(function (req, res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET,POST');
    res.setHeader('Access-Control-Allow-Headers', '*');

    redisClient.get('greeting', function(error, result) {
	  	if (error) {
	  		res.writeHead(500, {'Content-Type': 'text/html'});
            res.end("Internal Server Error");
	  		throw error;
	  	}
	  	res.writeHead(200, {'Content-Type': 'text/html'});
		res.end(result);
	  	console.log('GET result ->', result)
	});
  
}).listen(port);
console.log("Running server at port "+port);


