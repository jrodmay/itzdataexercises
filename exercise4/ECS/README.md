# ITZData Exercise 4

Exercise consists on having a NodeJS application that will run on 2 nodes, load balancing managed by NGINX, service discovery managed by CONSUL and storage managed by REDIS.

## How to deploy app to ECS using FARGATE 

### Tag Docker Images and Push them to your AWS account using the following script

NOTE: You must have ECS-CLI installed and configured on your local machine

```
./tag-push.sh
```

### Deploy the CloudFormation Template on your AWS account

Use the template CloudformationTemplate.json remember to change the docker image url for your own

### Test on web

Open your favorite browser and navigate to the IP (Public IP) obtained in "NETWORK" for the current task. Ex.

```
http://35.172.182.118/
```